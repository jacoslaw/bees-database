INSERT INTO colony (id, birth, name, hive)
VALUES ('1',  '2012-05-01', "Maja", 1);
INSERT INTO colony (id, parent_id, birth, name, hive)
VALUES ('2', '1',  '2014-06-11', "Lonia", 2);
INSERT INTO colony (id, parent_id, birth, name, hive)
VALUES ('3', '2', '2015-06-07', "Saba", 4);
INSERT INTO colony (id, birth, name, hive)
VALUES ('4', '2014-06-11', "Kiki", 3);


INSERT INTO hive 
VALUES ('1', '16', '6', 0);
INSERT INTO hive 
VALUES ('2', '17', '4', 0);
INSERT INTO hive 
VALUES ('3', '11', '7', 1);
INSERT INTO hive 
VALUES ('4', '15', '4', 1);


INSERT INTO harvest 
VALUES ('1', '2016-06-01', '3.5');
INSERT INTO harvest 
VALUES ('2', '2016-06-01', '4');
INSERT INTO harvest 
VALUES ('3', '2016-06-01', '2');
INSERT INTO harvest 
VALUES ('4', '2016-06-01', '3');


INSERT INTO feeding 
VALUES ('1', '2015-09-01', '2.6');
INSERT INTO feeding 
VALUES ('2', '2015-09-02', '3');
INSERT INTO feeding 
VALUES ('3', '2015-09-02', '1.5');
INSERT INTO feeding 
VALUES ('4', '2015-09-03', '2');


INSERT INTO inspection 
VALUES ('1', '2016-04-07', '2.6');
INSERT INTO inspection 
VALUES ('2', '2015-04-05', '3');
INSERT INTO inspection  
VALUES ('3', '2015-04-08', '1.5');
INSERT INTO inspection 
VALUES ('4', '2015-04-07', '2');

