
-- colony is identified by queen and her workers
CREATE TABLE colony (
  id INTEGER PRIMARY KEY NOT NULL,
  parent_id INTEGER,
  birth DATE NOT NULL,

  name CHAR(128) NOT NULL DEFAULT "",
  hive INTEGER UNIQUE,

  FOREIGN KEY (parent_id) REFERENCES colony(id),
  FOREIGN KEY (hive) REFERENCES hive(id) 
);


CREATE TABLE hive (
  id INTEGER PRIMARY KEY NOT NULL,
  -- total frames count
  frames_count INTEGER CHECK (frames_count >= 0) NOT NULL,
  -- how many frames are in nest
  nest_size INTEGER NOT NULL CHECK (frames_count >= nest_size),
  -- extra box with frames
  has_super BOOLEAN DEFAULT FALSE NOT NULL
);

CREATE TABLE inspection (
  colony_id INTEGER NOT NULL,
  performed DATE NOT NULL,
  notes TEXT DEAULT "" NOT NULL DEFAULT "",

  FOREIGN KEY (colony_id) REFERENCES colony(id) 
  PRIMARY KEY (colony_id, performed)
);

CREATE TABLE harvest (
  colony_id INTEGER NOT NULL,
  performed DATE NOT NULL,
  -- weight of the honey in kg
  weight INTEGER CHECK (weight >= 0) NOT NULL,

  FOREIGN KEY (colony_id) REFERENCES colony(id),
  PRIMARY KEY (colony_id, performed)
);

CREATE TABLE feeding (
  colony_id INTEGER NOT NULL,
  performed DATE NOT NULL,
  -- weight of the sugar in kg
  weight INTEGER CHECK (weight >= 0) NOT NULL,
  
  FOREIGN KEY (colony_id) REFERENCES colony(id),
  PRIMARY KEY (colony_id, performed)
);

--Creating a selection list
--CREATE TYPE hygiene_practice AS ENUM ('apiwarol', 'formic-acid', 'hive-cleaning', 'drone-brood-removal');  

CREATE TABLE hygiene (
  colony_id INTEGER NOT NULL,
  performed DATE NOT NULL,
  practice HYGIENE_PRACTICE NOT NULL,
  
  FOREIGN KEY (colony_id) REFERENCES colony(id),
  PRIMARY KEY (colony_id, performed, practice)
);



---------------------------------------------------------------------



INSERT INTO colony (id, birth, name, hive)
VALUES ('1',  '2012-05-01', "Maja", 1);
INSERT INTO colony (id, parent_id, birth, name, hive)
VALUES ('2', '1',  '2014-06-11', "Lonia", 2);
INSERT INTO colony (id, parent_id, birth, name, hive)
VALUES ('3', '2', '2015-06-07', "Saba", 4);
INSERT INTO colony (id, birth, name, hive)
VALUES ('4', '2014-06-11', "Kiki", 3);


INSERT INTO hive 
VALUES ('1', '16', '6', 0);
INSERT INTO hive 
VALUES ('2', '17', '4', 0);
INSERT INTO hive 
VALUES ('3', '11', '7', 1);
INSERT INTO hive 
VALUES ('4', '15', '4', 1);


INSERT INTO harvest 
VALUES ('1', '2016-06-01', '3.5');
INSERT INTO harvest 
VALUES ('2', '2016-06-01', '4');
INSERT INTO harvest 
VALUES ('3', '2016-06-01', '2');
INSERT INTO harvest 
VALUES ('4', '2016-06-01', '3');


INSERT INTO feeding 
VALUES ('1', '2015-09-01', '2.6');
INSERT INTO feeding 
VALUES ('2', '2015-09-02', '3');
INSERT INTO feeding 
VALUES ('3', '2015-09-02', '1.5');
INSERT INTO feeding 
VALUES ('4', '2015-09-03', '2');


INSERT INTO inspection 
VALUES ('1', '2016-04-07', '2.6');
INSERT INTO inspection 
VALUES ('2', '2015-04-05', '3');
INSERT INTO inspection  
VALUES ('3', '2015-04-08', '1.5');
INSERT INTO inspection 
VALUES ('4', '2015-04-07', '2');

---------------------------------------------------------------------

CREATE VIEW rodzina
AS SELECT * FROM colony;

CREATE VIEW ul
AS SELECT * FROM hive;

CREATE VIEW inspekcja
AS SELECT * FROM inspection;

CREATE VIEW miodobranie
AS SELECT * FROM harvest;

CREATE VIEW karmienie
AS SELECT * FROM feeding;

CREATE VIEW higiena
AS SELECT * FROM hygiene;








